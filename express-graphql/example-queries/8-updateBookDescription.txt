# A mutation example

mutation {
  updateBookDescription(id: 1, description: "A book about GraphQL") {
   ...bookFields  
  }
}

fragment bookFields on Book {
  title
  author
  description
  topic
  url
}