# A simple query example

query {
  getBookById(id: 1) {
		id
    title
    author
    url
  }  
}