# Index

  1. [Project description](#project-description)
  2. [Prerequisites](#prerequisites)
  3. [Project structure](#project-structure)
  4. [Versioning](#versioning)
  5. [Project configurations](#project-configurations)
  6. [Dependencies](#dependencies)
  7. [Installing dependencies](#installing-dependencies)
  8. [Execution](#execution)
  9. [Accessing](#accessing)

<br/>

---
## Project description:

This sub-project is the basic example of how to setup a [GraphQL][graphql_url] server just using [Express][express_url] as web server. This example serves as an introductory source code for what is a GraphQL API and their main components: [Schemas][graphql_schemas_url] and [Resolvers][graphql_resolvers_url].

<br/>

---
### Prerequisites

The first mandatory prerequisite is having [Node][node_url] installed. At the moment of creating this project, the latest Node version was _v11.11.0_.

The second mandatory prerequisite is having [NPM][npm_url] installed. At the moment of creating this project, the latest NPM version was _v6.7.0_.

A good recomendation is to use [NVM][nvm_url] as Node version manager.

<br/>

---
### Project structure

The `package.json` file has the project's configurations (see [project configurations](#project-configurations) section).

The `src` folder contains the source code of the project. As an introductory project, all the Javascript files (_.js_) are at the same level just to keep a simplier visibility of the project's components.

The `example-queries` folder contains some _.txt_ files with example queries that can be performed in the GraphQL UI provided as part of this template, or even using [cURL][curl_url].

<br/>

---
### Versioning

The project's version (see [project configurations](#project-configurations) section) is defined by using the [SemVer][semver_url] specification.

<br/>

---
### Project configurations

The file `package.json` holds the project's configurations such as: 

_Project's name, version, and description:_
```json
{
   "name": "express-graphql-demo",
   "version": "1.0.0",
   "description": "A simple demo of GraphQL using Express"
   ...
}
```

_Main JS file:_
```json
{
   ...
   "main": "src/server.js"
   ...
}
```

_Project's dependencies:_
```json
{
   ...
   "dependencies": {
      "express": "^4.16.4",
      "express-graphql": "^0.7.1",
      "graphql": "^14.1.1"
   }
}

```
<br/>
<br/>

---
### Dependencies
Dependencies are declared in the `package.json` file, under the _`dependencies`_ property.

| Dependency        | URL                                           |
| ------            | ------                                        |
| express           | https://www.npmjs.com/package/express         |
| graphql           | https://www.npmjs.com/package/graphql         |
| express-graphql   | https://www.npmjs.com/package/express-graphql |

<br/>

---
### Installing dependencies

In order to install dependencies, run the following [npm][npm_url] command at the _root_ folder: `npm install`.

_Example:_
```sh
graphql-javascript-demo/express-graphql:~$ npm install
```

<br/>

---
### Execution

Once the dependencies are installed, the template can be executed by running the following [Node][node_url] command at the _root_ folder: `node src/server.js`.

_Example:_
```sh
graphql-javascript-demo/express-graphql:~$ node src/server.js
```

<br/>

---
### Accessing
Using the default configuration of this template, the GraphQL API can be tested by using an UI provided by [GraphiQL][graphiql_url] in the following URL: `http://localhost:3000/graphql`.

<br/>

---

[graphql_url]: https://graphql.org/
[javascript_url]: https://www.javascript.com/
[express_url]: https://expressjs.com/
[node_url]: https://nodejs.org
[npm_url]: https://www.npmjs.com/
[nvm_url]: http://nvm.sh
[graphiql_url]: https://github.com/graphql/graphiql
[apollo_server_url]: https://www.apollographql.com/docs/apollo-server/
[apollo_playground_url]: https://www.apollographql.com/docs/apollo-server/features/graphql-playground.html
[apollo_server_express_url]: https://github.com/apollographql/apollo-server/tree/master/packages/apollo-server-express
[babel_url]: https://babeljs.io/
[semver_url]: https://semver.org/
[curl_url]: https://curl.haxx.se/
[graphql_schemas_url]: https://graphql.org/learn/schema/
[graphql_resolvers_url]: https://graphql.org/learn/execution/#root-fields-resolvers
[ecma_url]: https://www.ecma-international.org/
[nodemon_url]: https://nodemon.io/
[babel_node_cli_url]: https://babeljs.io/docs/en/babel-node
[cors_url]: https://www.npmjs.com/package/cors
[helmet_url]: https://www.npmjs.com/package/helmet
[schema_stitching_url]: https://www.apollographql.com/docs/graphql-tools/schema-stitching.html