
/**
 * Data: 
 * This is just to emulate some data. In a real-world application, data probably will come from a DB or another kind of source.
 */
const { books } = require('./data.json')

/**
 * Resolvers: 
 * They perform any action needed to process an user request. In real-world scenarios, resolvers probably will connect with a DB or 
 * any other backend application for getting/modifying/creating/deleting data.
 */
const resolvers = {
    /**
     * Queries are meant for getting data. Nothing prevents query-resolvers from modifying/creating/deleting data, but this goes 
     * against the design recommendation.
     */
    queries: {
        getBook: (args) => {
            return books.filter( book => book.id === args.id )[0]
        },
        getBooks: (args) => {
            if(args.title) {
                return books.filter( book => book.title.toLowerCase().includes(args.title.toLowerCase()) && args.title.length > 2 )
            }
            else if(args.author) {
                return books.filter( book => book.author.toLowerCase().includes(args.author.toLowerCase()) && args.author.length > 2 )
            }
            else if(args.topic) {
                return books.filter( book => book.topic.toLowerCase().includes(args.topic.toLowerCase()) && args.topic.length > 2 )
            } else {
                return books
            }
        },
    },
    /**
     * Mutations are meant for modifying/creating/deleting data.
     */
    mutations: {
        createBook: (args) => {
            let newBook = {
                id: Math.max(...books.map( book => book.id ), 0) + 1,
                title: args.title,
                author: args.author,
                description: args.description,
                topic: args.topic,
                url: args.url
            }
            books.push(newBook)
            return newBook;
        },
        updateBookDescription: (args) => {
            books.map( book => {
                if (book.id === args.id) {
                    book.description = args.description
                }
            } )
            return resolvers.queries.getBook( { id: args.id } )
        },
    },
}

module.exports = { resolvers }