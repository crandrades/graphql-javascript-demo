const { buildSchema } = require('graphql')

/**
 * Schema: 
 * It uses GraphQL's SDL (Schema Definition Language) to declare what functionalities are available 
 * through the GraphQL API, and what are the input/output structures for each functionality.
 */
const schema = buildSchema(`
type Query {
    getAllBooks: [Book]!
    getBookById(id: Int!): Book!
    getBooksByTitle(title: String!): [Book]!
    getBooksByAuthor(author: String!): [Book]!
    getBooksByTopic(topic: String!): [Book]!
}

type Mutation {
    createBook(title: String!, author: String!, description: String, topic: String!, url: String): Book!
    updateBookDescription(id: Int!, description: String!): Book!
}

type Book {
    id: Int
    title: String
    description: String
    author: String
    topic: String
    url: String
}
`)

module.exports = { schema }