const express = require('express')
const express_graphql = require('express-graphql')

/**
 * Schema
 */
const schema = require('./schema').schema

/**
 * Resolvers
 */
const resolvers = require('./resolvers').resolvers

/**
 * Root
 */
const root = {
    getAllBooks: resolvers.queries.getBooks,
    getBookById: resolvers.queries.getBook,
    getBooksByTitle: resolvers.queries.getBooks,
    getBooksByAuthor: resolvers.queries.getBooks,
    getBooksByTopic: resolvers.queries.getBooks,
    createBook: resolvers.mutations.createBook,
    updateBookDescription: resolvers.mutations.updateBookDescription
}

/**
 * GraphQL server configuration
 */
const route = 'graphql'
const port = 3000
const server = express()

server.use(`/${route}`, express_graphql({
    schema: schema,
    rootValue: root,
    graphiql: true // In-browser IDE for using GraphQL
}))

/**
 * Express server start up
 */
server.listen(port, () => { 
    console.log(`Server running. See http://localhost:${port}/${route}`) 
})