# Index

 1. [Project description](#project-description)
 2. [Sub-projects](#sub-projects)
      * [express-graphql](./express-graphql)
      * [apollo-graphql](./apollo-graphql)
      * [apollo-middleware-graphql](./apollo-middleware-graphql)
      * [apollo-middleware-auth-graphql](./apollo-middleware-auth-graphql)

<br/>

---
## Project description

This project contains three templates for [GraphQL][graphql_url] APIs using [Javascript][javascript_url] for the source code. The first of them simply shows how to setup a GraphQL server by using [Express][express_url], while the last two sub-projects use [Apollo Server][apollo_server_url] to build a little more robust GraphQL server.

The goal of these three examples is to show how easy is to build a GraphQL server application, so they ommit the use of a database or any other more complex integration.

<br/>

---
## Sub-projects

1. [Express GraphQL](./express-graphql): Introductory demo presenting the components of GraphQL APIs in a high level.
2. [Apollo GraphQL](#./apollo-graphql): Demo presenting the use of Apollo Server for creating a GraphQL API.
3. [Apollo GraphQL with middleware](./apollo-middleware-graphql): Demo presenting the use of middleware in a server built with Apollo Server.
4. [Apollo GraphQL with middleware for authorization](./apollo-middleware-auth-graphql): Demo presenting the use of JWT token in the GraphQL API for authentication and authorization.

<br/>

---

[graphql_url]: https://graphql.org/
[javascript_url]: https://www.javascript.com/
[express_url]: https://expressjs.com/
[node_url]: https://nodejs.org
[npm_url]: https://www.npmjs.com/
[nvm_url]: http://nvm.sh
[graphiql_url]: https://github.com/graphql/graphiql
[apollo_server_url]: https://www.apollographql.com/docs/apollo-server/
[apollo_playground_url]: https://www.apollographql.com/docs/apollo-server/features/graphql-playground.html
[apollo_server_express_url]: https://github.com/apollographql/apollo-server/tree/master/packages/apollo-server-express
[babel_url]: https://babeljs.io/
[semver_url]: https://semver.org/
[curl_url]: https://curl.haxx.se/
[graphql_schemas_url]: https://graphql.org/learn/schema/
[graphql_resolvers_url]: https://graphql.org/learn/execution/#root-fields-resolvers
[ecma_url]: https://www.ecma-international.org/
[nodemon_url]: https://nodemon.io/
[babel_node_cli_url]: https://babeljs.io/docs/en/babel-node
[cors_url]: https://www.npmjs.com/package/cors
[helmet_url]: https://www.npmjs.com/package/helmet
[schema_stitching_url]: https://www.apollographql.com/docs/graphql-tools/schema-stitching.html