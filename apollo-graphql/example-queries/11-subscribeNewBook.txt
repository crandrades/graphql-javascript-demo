subscription {
  newBook {
    id
    title
    author
    description
    topic
    url
  }
}