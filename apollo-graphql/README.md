# Index

  1. [Project description](#project-description)
  2. [Prerequisites](#prerequisites)
  3. [Project structure](#project-structure)
  4. [Versioning](#versioning)
  5. [Project configurations](#project-configurations)
  6. [Dependencies](#dependencies)
  7. [Installing dependencies](#installing-dependencies)
  8. [Execution](#execution)
  9. [Accessing](#accessing)

<br/>

---

## Project description:

This sub-project is a little more complex example of how to setup a [GraphQL][graphql_url] server. It uses [Apollo Server][apollo_server_url], a more robust solution for _production-ready_ GraphQL servers. We recommend to try the [express-graphql](../express-graphql) example first, in order to comprehend how a GraphQL server works.

<br/>

---
### Prerequisites

The first mandatory prerequisite is having [Node][node_url] installed. At the moment of creating this project, the latest Node version was _v11.11.0_.

The second mandatory prerequisite is having [NPM][npm_url] installed. At the moment of creating this project, the latest NPM version was _v6.7.0_.

A good recomendation is to use [NVM][nvm_url] as Node version manager.

<br/>

---
### Project structure

The `package.json` file has the project's configurations (see [project configurations](#project-configurations) section).

The `.babelrc` file configures a preset for [Babel][babel_url] transpiler. This allows to writen code slighty different of what an [ECMAScript][ecma_url] requires. The transpilation process will convert the source code into a standardized target code.

The `src` folder contains the source code of the project. The Javascript file (_.js_) are separated into two sub-folder: _resolvers_ and _schema_ (the goal of each sub-folder is evident). Within the _schema_ sub-folder there is a _**schema.graphql**_ file which holds the GraphQL schema. Instead as it was managed in the [express-graphql](../express-graphql) example, having the GraphQL schema as an external _.graphql_ file is a more realistic solution.

The `example-queries` folder contains some _.txt_ files with example queries that can be performed in the GraphQL UI provided as part of this template, or even using [cURL][curl_url].

<br/>

---
### Versioning

The project's version (see [project configurations](#project-configurations) section) is defined by using the [SemVer][semver_url] specification.

<br/>

---
### Project configurations

The file `package.json` holds the project's configurations such as: 

_Project's name, version, and description:_
```json
{
   "name": "apollo-graphql-demo",
   "version": "1.0.0",
   "description": "A simple demo of GraphQL using Apollo server"
   ...
}
```

_Main JS file:_
```json
{
   ...
   "main": "src/server.js"
   ...
}
```

_NPM scripts:_
```json
{
   ...
   "scripts": {
      "start": "nodemon src/server.js --watch src --exec babel-node"
   }
   ...
}
```

_Project's dependencies:_
```json
{
   ...
   "dependencies": {
      "apollo-server": "^2.4.8",
      "graphql": "^14.1.1",
      "graphql-tools": "^4.0.4"
   }
   ...
}
```

_Project's development dependencies:_
```json
{
   ...
   "devDependencies": {
      "@babel/core": "^7.3.4",
      "@babel/node": "^7.2.2",
      "@babel/preset-env": "^7.3.4",
      "nodemon": "^1.18.10"
   }
}
```

<br/>

---
### Dependencies
Dependencies are declared in the `package.json` file, under the _`dependencies`_ property.

| Dependency        | URL                                             |
| ------            | ------                                          |
| apollo-server     | https://www.npmjs.com/package/apollo-server     |
| graphql           | https://www.npmjs.com/package/graphql           |
| graphql-tools     | https://www.npmjs.com/package/graphql-tools     |
| nodemon           | https://www.npmjs.com/package/nodemon           |
| @babel/core       | https://www.npmjs.com/package/@babel/core       |
| @babel/node       | https://www.npmjs.com/package/@babel/node       |
| @babel/preset-env | https://www.npmjs.com/package/@babel/preset-env |

<br/>

---
### Installing dependencies

In order to install dependencies, run the following [npm][npm_url] command at the _root_ folder: `npm install`.

_Example:_
```sh
graphql-javascript-demo/apollo-graphql:~$ npm install
```
<br/>
<br/>

---
### Execution

Once the dependencies are installed, the template can be executed by running the following [Npm][npm_url] command at the _root_ folder: `npm run start`.

_Example:_
```sh
graphql-javascript-demo/apollo-graphql:~$ npm run start
```

<br/>

Note how this _**start**_ script is defined in the `package.json` file: 
```json
{
   ...
   "scripts": {
      "start": "nodemon src/server.js --watch src --exec babel-node"
   }
   ...
}
```

<br/>

The command will run the application with [nodemon][nodemon_url] after a [Babel][babel_url] transpilation. It will run _src/server.js_ as the main script, and will keep watching for any change under the `src` folder, in which case it will reload the transpilation and startup process.

<br/>

---
### Accessing

Using the default configuration of this template, the GraphQL API can be tested by using the [Apollo Playground][apollo_playground_url] UI in the following URL: `http://localhost:4000/`.

Subscriptions will be available at `ws://localhost:4000/`.

<br/>

---

[graphql_url]: https://graphql.org/
[javascript_url]: https://www.javascript.com/
[express_url]: https://expressjs.com/
[node_url]: https://nodejs.org
[npm_url]: https://www.npmjs.com/
[nvm_url]: http://nvm.sh
[graphiql_url]: https://github.com/graphql/graphiql
[apollo_server_url]: https://www.apollographql.com/docs/apollo-server/
[apollo_playground_url]: https://www.apollographql.com/docs/apollo-server/features/graphql-playground.html
[apollo_server_express_url]: https://github.com/apollographql/apollo-server/tree/master/packages/apollo-server-express
[babel_url]: https://babeljs.io/
[semver_url]: https://semver.org/
[curl_url]: https://curl.haxx.se/
[graphql_schemas_url]: https://graphql.org/learn/schema/
[graphql_resolvers_url]: https://graphql.org/learn/execution/#root-fields-resolvers
[ecma_url]: https://www.ecma-international.org/
[nodemon_url]: https://nodemon.io/
[babel_node_cli_url]: https://babeljs.io/docs/en/babel-node
[cors_url]: https://www.npmjs.com/package/cors
[helmet_url]: https://www.npmjs.com/package/helmet
[schema_stitching_url]: https://www.apollographql.com/docs/graphql-tools/schema-stitching.html