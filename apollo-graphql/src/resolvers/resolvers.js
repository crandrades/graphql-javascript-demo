import { PubSub } from 'apollo-server';

// Topic and topic publication/subscription agent
const pubsub = new PubSub();
const NEW_BOOK_TOPIC = 'newBookTopic';

/**
 * Data: 
 * This is just to emulate some data. In a real-world application, data probably will come from a DB or another kind of source.
 */
const { books } = require('../../resources/data.json');

/**
 * Resolvers: 
 * They perform any action needed to process an user request. In real-world scenarios, resolvers probably will connect with a DB or 
 * any other backend application for getting/modifying/creating/deleting data.
 */
const resolvers = {
    /**
     * Queries are meant for getting data. Nothing prevents query-resolvers from modifying/creating/deleting data, but this goes 
     * against the design recommendation.
     */
    Query: {
        getAllBooks: (_) => {
            return books;
        },
        getBookById: (_, args) => {
            //Note how the resolver returns the entire object, whose properties will be filtered by the server as the query requires
            return books.filter( book => book.id === args.id )[0];
        },
        getBooksByTitle: (_, args) => {
            return books.filter( book => book.title.toLowerCase().includes(args.title.toLowerCase()) && args.title.length > 2 );
        },
        getBooksByAuthor: (_, args) => {
            return books.filter( book => book.author.toLowerCase().includes(args.author.toLowerCase()) && args.author.length > 2 );
        },
        getBooksByTopic: (_, args) => {
            return books.filter( book => book.topic.toLowerCase().includes(args.topic.toLowerCase()) && args.topic.length > 2 );
        },
    },
    /**
     * Mutations are meant for modifying/creating/deleting data.
     */
    Mutation: {
        createBook: (_, args) => {
            let newBook = {
                id: Math.max(...books.map( book => book.id ), 0) + 1,
                title: args.title,
                author: args.author,
                description: args.description,
                topic: args.topic,
                url: args.url
            };
            pubsub.publish(NEW_BOOK_TOPIC, { newBook: newBook }); //Publish the event in the corresponding topic
            books.push(newBook);
            return newBook;
        },
        updateBookDescription: (_, args) => {
            books.map( book => {
                if (book.id === args.id) {
                    book.description = args.description;
                }
            } )
            return resolvers.queries.getBook( { id: args.id } );
        },
    },
    /**
     * Mutations are meant forsubscribing to events e.g., data modification/creation/deletion.
     */
    Subscription: {
        newBook: {
            subscribe: () => pubsub.asyncIterator([NEW_BOOK_TOPIC]), //Subscribes to the 'new book' topic for getting new events
        }
    }
};

module.exports = { resolvers };