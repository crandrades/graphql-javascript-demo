import { makeExecutableSchema } from 'graphql-tools';
import { resolvers } from '../resolvers/resolvers'
import path from 'path';
import fs from 'fs';

// Read schema from external file
const typeDefs = fs.readFileSync(path.join(__dirname, "schema.graphql"), "utf8");

/**
 * Schema: 
 * It uses GraphQL's SDL (Schema Definition Language) to declare what functionalities are available 
 * through the GraphQL API, and what are the input/output structures for each functionality.
 */
const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
});

module.exports = { schema }