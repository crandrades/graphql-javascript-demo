import { schema as graphqlSchema } from './schema/schema';
import { ApolloServer } from 'apollo-server';

/**
 * Apollo Server
 */
const server = new ApolloServer({
    schema: graphqlSchema,
    introspection: true,
    playground: true,
});

/**
 * Server run
 */
server.listen().then(({ url }) => {
    console.log(`Server ready at ${url}`);
});