import { schema as graphqlSchema } from './schema/schema';
import { users } from '../resources/users.json'
import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import http from 'http';
import cors from 'cors';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

/**
 * GraphQL server configuration
 */
const PORT = 4000;
const GRAPHQL_API_PATH = '/graphqlAPI';
const AUTH_API_PATH = '/get-token';

/**
 * Middleware declaration
 */
const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use(helmet());

// JWT sign key. In a real-world scenario, this constant must be handled with higher security.
const SECRET_KEY = '$C&F)J@NcRfUjXnZr4u7x!A%D*G-KaPd'; //256-bit

/**
 * Authentication interface for getting JWT authorization token
 */
app.post(AUTH_API_PATH, async (req, res) => {
    const { email, password } = req.body;
    if (email && password) {
        const user = users.find(user => user.email === email);
        if (user) { 
            // Use bcrypt to compare the hash in the DB (resources/users.js) with the password the user provides
            const match = await bcrypt.compare(password, user.password);
            if (match) {
                //Create a JWT for the user with our secret encrypting some user data
                const token = jwt.sign({ email: user.email, id: user.id }, SECRET_KEY);
                res.send({
                    success: true,
                    token: token,
                });
            } else {
                // Return error to user to let them know the password is incorrect
                res.status(401).send({
                    success: false,
                    message: 'Incorrect credentials.',
                });
            }
        } else {
            // Return error to user to let them know the account does not exists
            res.status(404).send({
                success: false,
                message: `Could not find account for ${email}.`,
            });
        }
    } else {
        // Return error to user to let them know the request is missing some required data
        res.status(400).send({
            success: false,
            message: `Bad request. Please provide email and password.`,
        });
    }
});

/**
 * Creating our Apollo context function
 */
const context = ({ req }) => {
    // Get the user token from the headers, or empty by default
    const token = req.headers.authorization || '';
    const splitToken = token.split(' ')[1];
    try {
        jwt.verify(splitToken, SECRET_KEY)
    } catch (e) {
        throw new AuthenticationError(
            'Authentication token is invalid, please log in',
        )
    }
}

/**
 * Apollo Server
 */
const server = new ApolloServer({
    schema: graphqlSchema,
    introspection: true,
    playground: true,
    subscriptions: {
        path: '/subscriptions'
    },
    context: context
});

/**
 * Apply middleware to Apollo Server
 */
server.applyMiddleware({
    app,
    path: GRAPHQL_API_PATH
});

/**
 * Subscription declaration
 */
const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

/**
 * Server run
 */
httpServer.listen(PORT, () => {
    console.log(`Server ready at http://localhost:${PORT}${server.graphqlPath}`);
    console.log(`Subscriptions ready at ws://localhost:${PORT}${server.subscriptionsPath}`);
    console.log(`Authorization POST endpoint ready at http://localhost:${PORT}${AUTH_API_PATH}`);
});