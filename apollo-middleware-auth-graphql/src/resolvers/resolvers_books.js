import { PubSub } from 'apollo-server';

// Topic and topic publication/subscription agent
const pubsub = new PubSub();
const NEW_BOOK_TOPIC = 'newBookTopic';

/**
 * Data: 
 * This is just to emulate some data. In a real-world application, data probably will come from a DB or another kind of source.
 */
const { books } = require('../../resources/data.json');

/**
 * Resolvers: 
 * They perform any action needed to process an user request. In real-world scenarios, resolvers probably will connect with a DB or 
 * any other backend application for getting/modifying/creating/deleting data.
 */
const resolvers = {
    /**
     * Queries are meant for getting data. Nothing prevents query-resolvers from modifying/creating/deleting data, but this goes 
     * against the design recommendation.
     */
    Query: {
        getAllBooks: (_) => {
            return books;
        },
        getBookById: (_, args) => {
            //Note how the resolver returns the entire object, whose properties will be filtered by the server as the query requires
            return books.filter( book => book.id === args.id )[0];
        },
        getBooksByTitle: (_, args) => {
            return books.filter( book => {
                return args.title.trim().length > 2 && 
                        book.title.toLowerCase().includes(args.title.toLowerCase());
            });
        },
        getBooksByAuthor: (_, args) => {
            return books.filter( book => {
                let queryWords = args.author.trim().toLowerCase().split(" ");
                return args.author.trim().length > 2 &&
                        book.authors.some( author => {
                            return queryWords.includes(author.name.trim().toLowerCase()) ||
                                    queryWords.includes(author.surname.trim().toLowerCase())
                        });
            })
        },
        getBooksByTopic: (_, args) => {
            return books.filter( book => {
                return args.topic.trim().length > 2 && 
                        book.topic.toLowerCase().includes(args.topic.toLowerCase());
            });
        },
    },
    /**
     * Mutations are meant for modifying/creating/deleting data.
     */
    Mutation: {
        createBook: (_, args) => {
            let newBook = {
                id: Math.max(...books.map( book => book.id ), 0) + 1,
                title: args.title,
                authors: [],
                description: args.description,
                topic: args.topic,
                url: args.url
            };
            args.authors.forEach(author => newBook.authors.push({ name: author.name, surname: author.surname, email: author.email }));
            books.push(newBook);
            pubsub.publish(NEW_BOOK_TOPIC, { newBook: newBook });
            return newBook;
        },
        updateBookDescription: (_, args) => {
            books.map( book => {
                if (book.id === args.id) {
                    book.description = args.description;
                }
            } )
            return resolvers.queries.getBook( { id: args.id } );
        },
    },
    /**
     * Mutations are meant forsubscribing to events e.g., data modification/creation/deletion.
     */
    Subscription: {
        newBook: {
            subscribe: () => pubsub.asyncIterator([NEW_BOOK_TOPIC]),
        },
    }
};

module.exports = { resolvers };