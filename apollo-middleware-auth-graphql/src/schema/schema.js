import { makeExecutableSchema, mergeSchemas } from 'graphql-tools';
import { resolvers as resolvers_books } from '../resolvers/resolvers_books'
import { resolvers as resolvers_authors } from '../resolvers/resolvers_authors'
import path from 'path';
import fs from 'fs';

// Read schemas from external files
const typeDefs_books = fs.readFileSync(path.join(__dirname, "schema_books.graphql"), "utf8");
const typeDefs_authors = fs.readFileSync(path.join(__dirname, "schema_authors.graphql"), "utf8");

/**
 * Schema: 
 * It uses GraphQL's SDL (Schema Definition Language) to declare what functionalities are available 
 * through the GraphQL API, and what are the input/output structures for each functionality.
 */
const booksSchema = makeExecutableSchema({
    typeDefs: typeDefs_books,
    resolvers: resolvers_books,
});

/**
 * Schema: 
 * It uses GraphQL's SDL (Schema Definition Language) to declare what functionalities are available 
 * through the GraphQL API, and what are the input/output structures for each functionality.
 */
const authorsSchema = makeExecutableSchema({
    typeDefs: typeDefs_authors,
    resolvers: resolvers_authors,
});

/**
 * Schema stitching: 
 * It merges two or more schemas into one. Be aware of entities using the same name.
 */
const schema = mergeSchemas({
    schemas: [
        booksSchema,
        authorsSchema,
    ],
});

module.exports = { schema }