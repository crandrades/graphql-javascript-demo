/**
 * Data: 
 * This is just to emulate some data. In a real-world application, data probably will come from a DB or another kind of source.
 */
const { books } = require('../../resources/data.json');

// Reorganize data for coding simplicity
let authors = [];
books.forEach( book => book.authors.forEach( author => {
        author.books = [book]
        authors.push(author)
    }) 
);

/**
 * Resolvers: 
 * They perform any action needed to process an user request. In real-world scenarios, resolvers probably will connect with a DB or 
 * any other backend application for getting/modifying/creating/deleting data.
 */
const resolvers = {
    /**
     * Queries are meant for getting data. Nothing prevents query-resolvers from modifying/creating/deleting data, but this goes 
     * against the design recommendation.
     */
    Query: {
        getAllAuthors: (_) => {
            return authors;
        },
        getAuthorsByName: (_, args) => {
            let queryWords = args.author.trim().toLowerCase().split(" ");
            return authors.filter( author => {
                            return args.author.trim().length > 2 &&
                                    queryWords.includes(author.name.trim().toLowerCase()) ||
                                    queryWords.includes(author.surname.trim().toLowerCase())
            });
        },
        getAuthorsByBooks: (_, args) => {
            return authors.filter( author => {
                return args.title.trim().length > 2 &&
                        author.books.some( book => {
                            return book.title.trim().toLowerCase().includes( args.title.trim().toLowerCase());
                        });
            })
        },
    },
};

module.exports = { resolvers };