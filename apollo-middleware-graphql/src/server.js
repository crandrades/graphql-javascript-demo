import { schema as graphqlSchema } from './schema/schema';
import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import http from 'http';
import cors from 'cors';
import helmet from 'helmet';

/**
 * GraphQL server configuration
 */
const PORT = 4000;
const API_PATH = '/graphqlAPI'

/**
 * Middleware declaration
 */
const app = express();
app.use(cors());
app.use(helmet());

/**
 * Apollo Server
 */
const server = new ApolloServer({
    schema: graphqlSchema,
    introspection: true,
    playground: true,
    subscriptions: {
        path: '/subscriptions'
    }
});

/**
 * Apply middleware to Apollo Server
 */
server.applyMiddleware({
    app,
    path: API_PATH
});

/**
 * Subscription declaration
 */
const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

/**
 * Server run
 */
httpServer.listen(PORT, () => {
    console.log(`Server ready at http://localhost:${PORT}${server.graphqlPath}`);
    console.log(`Subscriptions ready at ws://localhost:${PORT}${server.subscriptionsPath}`);
});