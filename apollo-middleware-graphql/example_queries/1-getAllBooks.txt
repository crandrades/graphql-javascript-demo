query {
  getAllBooks {
    id
    title
    authors {
      name
      surname
    }
    description
    topic
    url
  }
}