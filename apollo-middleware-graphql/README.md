# Index

  1. [Project description](#project-description)
  2. [Prerequisites](#prerequisites)
  3. [Project structure](#project-structure)
  4. [Versioning](#versioning)
  5. [Project configurations](#project-configurations)
  6. [Dependencies](#dependencies)
  7. [Installing dependencies](#installing-dependencies)
  8. [Execution](#execution)
  9. [Accessing](#accessing)

<br/>

---
## Project description:

This sub-project is the continuation of the second example (see [apollo-graphql](../apollo-graphql) project). It shows how to setup a [GraphQL][graphql_url] server by using [Apollo Server][apollo_server_url], but using [Express][express_url] as middleware and a few more utilities like [Cors][cors_url] and [Helmet][helmet_url].

It also shows relations between _schema entities_ and _schema stitching_ for combining multiple schemas into one (see [schema stitching][schema_stitching_url] in Apollo server).

<br/>

---
### Prerequisites

The first mandatory prerequisite is having [Node][node_url] installed. At the moment of creating this project, the latest Node version was _v11.11.0_.

The second mandatory prerequisite is having [NPM][npm_url] installed. At the moment of creating this project, the latest NPM version was _v6.7.0_.

A good recomendation is to use [NVM][nvm_url] as Node version manager.

<br/>

---
### Project structure

The `package.json` file has the project's configurations (see [project configurations](#project-configurations) section).

The `.babelrc` file configures a preset for [Babel][babel_url] transpiler. This allows to writen code slighty different of what an [ECMAScript][ecma_url] requires. The transpilation process will convert the source code into a standardized target code.

The `src` folder contains the source code of the project. The Javascript file (_.js_) are separated into two sub-folder: _resolvers_ and _schema_ (the goal of each sub-folder is evident). 
Within the _schema_ sub-folder there are two _.graphql_ files: _**schema_authors.graphql**_ and _**schema_books.graphql**_. Both files hold a GraphQL schema whose will be used for _schema stitching_ in the `src/schema/schema.js` script. Note that there should not be two types (_entities_) with the same name or one of them will be overwritten. That is the reason why we add a _Book_ or _Author_ prefix to each type as corresponds, in order to distinguish them.

The `example-queries` folder contains some _.txt_ files with example queries that can be performed in the GraphQL UI provided as part of this template, or even using [cURL][curl_url].

<br/>

---
### Versioning

The project's version (see [project configurations](#project-configurations) section) is defined by using the [SemVer][semver_url] specification.

<br/>

---
### Project configurations

The file `package.json` holds the project's configurations such as: 

_Project's name, version, and description:_
```json
{
   "name": "apollo-middleware-graphql-demo",
   "version": "1.0.0",
   "description": "A simple demo of GraphQL using Apollo server and some middleware"
   ...
}
```

_Main JS file:_
```json
{
   ...
   "main": "src/server.js"
   ...
}
```

_NPM scripts:_
```json
{
   ...
   "scripts": {
      "dev": "nodemon src/server.js --watch src --exec babel-node",
      "build": "babel src --out-dir dist --copy-files",
      "prod": "npm run build && node dist/server.js",
      "clean": "rm -rf package-lock.json dist/ node_modules/"
   }
   ...
}
```

_Project's dependencies:_
```json
{
   ...
   "dependencies": {
      "apollo-server": "^2.4.8",
      "apollo-server-express": "^2.4.8",
      "cors": "^2.8.5",
      "express": "^4.16.4",
      "graphql": "^14.1.1",
      "graphql-tools": "^4.0.4",
      "helmet": "^3.16.0"
   }
   ...
}
```

_Project's development dependencies:_
```json
{
   ...
   "devDependencies": {
      "@babel/cli": "^7.2.3",
      "@babel/core": "^7.3.4",
      "@babel/node": "^7.2.2",
      "@babel/preset-env": "^7.3.4",
      "nodemon": "^1.18.10"
   }
}
```

<br/>

---
### Dependencies
Dependencies are declared in the `package.json` file, under the _`dependencies`_ property.

| Dependencia           | URL                                                 |
| ------                | ------                                              |
| express               | https://www.npmjs.com/package/express               |
| apollo-server         | https://www.npmjs.com/package/apollo-server         |
| apollo-server-express | https://www.npmjs.com/package/apollo-server-express |
| graphql               | https://www.npmjs.com/package/graphql               |
| graphql-tools         | https://www.npmjs.com/package/graphql-tools         |
| cors                  | https://www.npmjs.com/package/cors                  |
| helmet                | https://www.npmjs.com/package/helmet                |
| nodemon               | https://www.npmjs.com/package/nodemon               |
| @babel/core           | https://www.npmjs.com/package/@babel/core           |
| @babel/node           | https://www.npmjs.com/package/@babel/node           |
| @babel/cli            | https://www.npmjs.com/package/@babel/cli            |
| @babel/preset-env     | https://www.npmjs.com/package/@babel/preset-env     |

<br/>

---
### Installing dependencies

In order to install dependencies, run the following [npm][npm_url] command at the _root_ folder: `npm install`.

_Example:_
```sh
graphql-javascript-demo/apollo-middleware-graphql:~$ npm install
```
<br/>
<br/>

---
### Execution

Once the dependencies are installed, the `package.json` file make available all of the following [Npm][npm_url] commands at the _root_ folder:

```json
{
   ...
   "scripts": {
      "dev": "nodemon src/server.js --watch src --exec babel-node",
      "build": "babel src --out-dir dist --copy-files",
      "prod": "npm run build && node dist/server.js",
      "clean": "rm -rf package-lock.json dist/ node_modules/"
   }
   ...
}
```

<br/>

- `npm run dev`: It uses [Babel][babel_url] to traspile the source code and [nodemon][nodemon_url] to run the application while watching any change under the `src` folder, in which case it will reload the transpilation and startup process.

   _Example:_
   ```sh
   graphql-javascript-demo/apollo-middleware-graphql:~$ npm run dev
   ```

<br/>

- `npm run build`: It uses [Babel][babel_url] to traspile the source code by using [Babel/node CLI][babel_node_cli_url]. It sets as the _target_ folder the `dist` folder while copying any file that is not a Javascript file (_.js_). This will copy any _.graphql_ file, for example. This command will not start the application, just 'compile' it.

   _Example:_
   ```sh
   graphql-javascript-demo/apollo-middleware-graphql:~$ npm run build
   ```

<br/>

- `npm run prod`: It runs the _build_ command and then starts the application compiled in the `dist` folder with [Node][node_url].

   _Example:_
   ```sh
   graphql-javascript-demo/apollo-middleware-graphql:~$ npm run prod
   ```

<br/>

- `npm run clean`: It deletes the `package-lock.json` file, the `dist` folder which contains the traspiled source code after a _build_ command execution, and the `node_modules` folder which contains the project's dependencies.

   _Example:_
   ```sh
   graphql-javascript-demo/apollo-middleware-graphql:~$ npm run clean
   ```

<br/>

---
### Accessing

Using the default configuration of this template, the GraphQL API can be tested by using the [Apollo Playground][apollo_playground_url] UI in the following URL: `http://localhost:4000/graphqlAPI`.

Subscriptions will be available at the `ws://localhost:4000/subscriptions` endpoint.

<br/>

---

[graphql_url]: https://graphql.org/
[javascript_url]: https://www.javascript.com/
[express_url]: https://expressjs.com/
[node_url]: https://nodejs.org
[npm_url]: https://www.npmjs.com/
[nvm_url]: http://nvm.sh
[graphiql_url]: https://github.com/graphql/graphiql
[apollo_server_url]: https://www.apollographql.com/docs/apollo-server/
[apollo_playground_url]: https://www.apollographql.com/docs/apollo-server/features/graphql-playground.html
[apollo_server_express_url]: https://github.com/apollographql/apollo-server/tree/master/packages/apollo-server-express
[babel_url]: https://babeljs.io/
[semver_url]: https://semver.org/
[curl_url]: https://curl.haxx.se/
[graphql_schemas_url]: https://graphql.org/learn/schema/
[graphql_resolvers_url]: https://graphql.org/learn/execution/#root-fields-resolvers
[ecma_url]: https://www.ecma-international.org/
[nodemon_url]: https://nodemon.io/
[babel_node_cli_url]: https://babeljs.io/docs/en/babel-node
[cors_url]: https://www.npmjs.com/package/cors
[helmet_url]: https://www.npmjs.com/package/helmet
[schema_stitching_url]: https://www.apollographql.com/docs/graphql-tools/schema-stitching.html